import { Component, OnInit, ViewChild } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import { ExportAsService, ExportAsConfig } from 'ngx-export-as';
import {Router, ActivatedRoute} from '@angular/router';

import {ClaimService} from '../../../../../services/http/admin/claim.service';
import {GeneralService} from '../../../../../services/general/general.service';


@Component({
  selector: 'app-claims',
  templateUrl: './claims.component.html',
  styleUrls: ['./claims.component.scss']
})
export class ClaimsComponent implements OnInit {

  displayedColumns = ['associateId', 'createdByName', 'associateNric', 'receiptAmount', 'status','receiptUrls', 'createdTime','approved'];
  dataSource;
  
  @ViewChild(MatSort) sort: MatSort;

  claimDtl;
  popupDetail = false;
  isFilter = false;
  isRejectClicked = false;
  data;
  pageLength=0;
  filterText='';
  filter='';
  myVar;
  sortBy ='';
  sortOrder = '';
  dataListFilter;
  amountData=[
    {value:[0,20],text: '$0 to $20'},
    {value:[20.01,50],text: '$20.01 to $50'},
    {value:[50.01,100],text: '$50.01 to $100'},
    {value:[100],text: 'above 100'},
  ];
  attachmenFilter=[
    "none","single",'multiple'
  ]

  applyFilter(filterValue: string, key) {
    clearTimeout(this.myVar);
    this.filterText = filterValue.trim().toLowerCase();
    if(key == 'Enter'){
      this.http_getClaimList(0);
    }else{
      this.myVar = setTimeout(() => { 
        this.http_getClaimList(0);
      }, 2000);
    }
  }

  setQueryParameter(searchText, filter){
    let param ={
      search:searchText,
      filter:filter
    }
    this.router.navigate([], { queryParams: param });
  }

  getQueryParam(){
    this.actRouter.queryParams.subscribe(params => {
      console.log(params );
    });
  }

  exportAsConfig: ExportAsConfig = {
    type: 'xls', // the type you want to download
    elementId: 'exportTable', // the id of html/table element
  }

  constructor(
    private exportAsService: ExportAsService,
    private _claim: ClaimService,
    private _general:GeneralService,
    private router:Router,
    private actRouter:ActivatedRoute
  ) { }

  ngOnInit() {
    this.http_getClaimList(0);
    this.actRouter
  }

  http_getClaimList(page){
    this._general.setLoading(true);
    let param ={
      page:page,
      numClaimsPerPage:  this.pageOpt != undefined? this.pageOpt.pageSize:this.pageSizeOptions[0],
      searchString :this.filterText,
      sortBy:this.sortBy,
      sortOrder:this.sortOrder,
      // filter:this.filterQuery()
      queryString:this.filterQuery()
    };
    this._claim.get(param).subscribe(
      (resp:anyObject)=>{
        this._general.setLoading(false);
        this.pageLength = resp.totalNumClaims;
        this.data = resp.claimList;
        this.dataListFilter=resp;
        this.dataSource = new MatTableDataSource(this.data);
        this.dataSource.sort = this.sort;
        
      },(err)=>{
        this._general.setLoading(false);
        console.log(err);
      }
    )
  }

  sortData(data){
    this.sortOrder= data.direction;
    this.sortBy=this.sortOrder == ''?'':data.active;
    this.http_getClaimList(0);
  }

  pageSizeOptions: number[] = [ 15, 50, 100];

  pageOpt;
  pageEvent(data){
    this.pageOpt = data;
    this.http_getClaimList(data.pageIndex );
  };

  isDataPrevAct = true;
  isDataNextAct = true;
  showClaimDetail(data){
    this.popupDetail = true;
    this.claimDtl = data;
  }

  prevData(){
    let id = this.checkArrayNumber();
    this.checkNextPrevActive(id-1);
    let prevId = this.data[id-1];
    this.claimDtl = prevId;
  }
  
  nextData(){
    let id = this.checkArrayNumber();
    this.checkNextPrevActive(id+1);
    let nextId = this.data[id+1];
    this.claimDtl = nextId;
  }

  checkNextPrevActive(idx){
    this.isDataPrevAct = idx==0?false:true;
    this.isDataNextAct = idx==this.data.length-1?false:true;
  }

  checkArrayNumber(){
    for (let i = 0; i < this.data.length; i++) {
      if(this.data[i].associateId == this.claimDtl.associateId){ 
        return i;
      }
    }
  }

  filterData(){
    this.isFilter = !this.isFilter;
  }

  isShowFilter={
    status:false,
    show:-1,
    isOutside: false
  }
  showFilter(data){
    this.isShowFilter.status = this.isShowFilter.show==data? !this.isShowFilter.status : true;
    this.isShowFilter.show = data;
    this.isShowFilter.isOutside = false;
  }

  cancelFilter(data:boolean){
    this.isShowFilter.status = data;
  }

  filterByassociateId=[];
  filterBycreatedByName=[];
  filterByNric=[]
  filterByAmount=[]
  filterByStatus=[]
  filterByAttachment=[]
  filterByDateSubmitted=[]
  filterByDateApproved=[]

  applyFilterData(data, type){
    if(type == 'associateId'){
      this.filterByassociateId = data;
    }
    if(type =='createdByName'){
      this.filterBycreatedByName = data;
    }
    if(type =='nric'){
      this.filterByNric = data;
    }
    if(type =='status'){
      this.filterByStatus = data;
    }
    if(type == 'submissionDate' ){
      let rtn = []
      for (let i = 0; i < data.length; i++) {
        let dt = new Date(data[i]);
        let mnt = dt.getMonth()+1;
        let dte = dt.getDate();
        let s = (dte>9?dte.toString():"0"+dte.toString())+"/"+(mnt>9?mnt.toString():"0"+mnt.toString())+"/"+dt.getFullYear();
        rtn=[...rtn,s];
      }
      this.filterByDateSubmitted = rtn;
    }
    if(type == 'approved' ){
      let rtn = []
      for (let i = 0; i < data.length; i++) {
        let dt = new Date(data[i]);
        let mnt = dt.getMonth()+1;
        let dte = dt.getDate();
        let s = (dte>9?dte.toString():"0"+dte.toString())+"/"+(mnt>9?mnt.toString():"0"+mnt.toString())+"/"+dt.getFullYear();
        rtn=[...rtn,s];
      }
      this.filterByDateApproved = rtn;
    }if(type == 'attachment'){
      this.filterByAttachment = data;
    }if(type == 'receiptAmount'){
      this.filterByAmount = data;
    }
    
    this.filterQuery();
    this.http_getClaimList(0);
  }
  

  filterQuery(){
    let asc =  this.filterByassociateId.length > 0?`associateId:${this.filterByassociateId.toString()};`:'';
    let name = this.filterBycreatedByName.length>0?`createdByName:${this.filterBycreatedByName.toString()};`:"";
    let nric = this.filterByNric.length>0?`associateNric:${this.filterByNric.toString()};`:"";
    let status = this.filterByStatus.length>0?`status:${this.filterByStatus.toString()};`:"";
    let submissionDate = this.filterByDateSubmitted.length>0?`createdTime:${this.filterByDateSubmitted.toString()};`:"";
    let approved = this.filterByDateApproved.length>0?`approveTime:${this.filterByDateApproved.toString()};`:"";
    let attachment = this.filterByAttachment.length>0?`receiptUrls:${this.filterByAttachment.toString()};`:"";
    let receiptAmount = this.filterByAmount.length>0?`receiptAmount:${this.filterByAmount.toString()};`:"";
    let res = asc+name+nric+submissionDate+approved+status+attachment+receiptAmount;
    
    return res;    
  }


  dataExportExcel;
  export() {
    this._general.setLoading(true);
    let param ={
      queryString:this.filterQuery()
    };
    this._claim.get(param).subscribe(
      (resp:anyObject)=>{
        this._general.setLoading(false);
        this.dataExportExcel = resp.claimList;
        setTimeout(()=> {this.exportExcel();}, 3000);
      },(err)=>{
        this._general.setLoading(false);
        console.log(err);
      }
    )
  }

  exportExcel(){
    this._general.setLoading(true);

    this.exportAsService.save(this.exportAsConfig, 'Claim List').subscribe(
      (ss) => {this._general.setLoading(false);},
      (err)=>{this._general.setLoading(false);});
  }

  popupDetPdf={
    show:false,
    source:''
  }

}


interface anyObject {
  [key: string]: any
}

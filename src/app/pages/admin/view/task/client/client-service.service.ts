import { Injectable } from '@angular/core';
import {GeneralService} from '../../../../../services/general/general.service';
import {ClientService} from '../../../../../services/http/admin/client.service';

@Injectable({
  providedIn: 'root'
})
export class ClientServiceService {

  constructor(
    private _general : GeneralService,
    private _client:ClientService,
  ) { }


  setDays(data){
    data = undefined;
    data = [];
    for (let i = 1; i < 32; i++) {
      data.push(i.toString()); 
    }
    return data;
  };

  setParam(data){
    let ss=[];
    for (let i = 0; i < data.length; i++) {
      
      let param = {
        probationPeriod: 
          data[i].probationPeriod == undefined?
          {value: "", unit: ""}: data[i].probationPeriod, 
        workingDaysPerWeek: 
          data[i].workingDaysPerWeek == undefined?
          ["","","","","","",""] : data[i].workingDaysPerWeek,
        workingHours:
          data[i].workingHours == undefined?
          {startTime: "", endTime: ""} : data[i].workingHours,
        restDays: 
          data[i].restDays == undefined?
          ["","","","","","",""]: data[i].restDays,
        variableOt1_0:
          data[i].variableOt1_0 == undefined?
          {
            active: false, 
            settings: [
              {
                "equality":"",
                "for": "",
                "numHours": ""
              }
            ]
          }
          :data[i].variableOt1_0,
        variableOt1_5:
          data[i].variableOt1_5 == undefined?
          {
            active: false, 
            settings: [
              {
                "equality":"",
                "for": "",
                "numHours": ""
              }
            ]
          }:data[i].variableOt1_5,
        variableOt2_0:
          data[i].variableOt2_0 == undefined?
          {
            active: false, 
            settings: [
              {
                "equality":"",
                "for": "",
                "numHours": ""
              }
            ]
          }:data[i].variableOt2_0 
          ,
        fixedOt: 
          data[i].fixedOt == undefined?
          [{
            active: false, 
            settings: {
              additionalValue: "",
              additionalValueUnit: "",
              per: ""
            }
          }]:data[i].fixedOt,
        completionBonusPayment: 
          data[i].completionBonusPayment == undefined?
          "": data[i].completionBonusPayment,
        payslipFormat: 
          data[i].payslipFormat == undefined?
          "":data[i].payslipFormat,
        changeTimeSetting:{
          start:{
            time:"",
            ampm:"",
          },
          end:{
            time:"",
            ampm:"",
          }
        },
        overtimePayEntitlement:data[i].overtimePayEntitlement,
        templateName:data[i].templateName,
        breakTime:data[i].breakTime,
        breakTimeFake:
        data[i].breakTime == 45 || data[i].breakTime == 60?data[i].breakTime:"other",
        breakTimeOther: data[i].breakTime == 0?null:data[i].breakTime
      }
      ss.push(param)
    }
    return ss;
  }

  setTemplate(data){
    for (let i = 0; i < data.length; i++) {
      data[i].templateName = data[i].templateName;
      data[i].variableOt1_0.settings.length == 0?this.addmore(data[i].variableOt1_0.settings):data[i].variableOt1_0.settings;
      data[i].variableOt1_5.settings.length == 0?this.addmore(data[i].variableOt1_5.settings):data[i].variableOt1_5.settings;
      data[i].variableOt2_0.settings.length == 0?this.addmore(data[i].variableOt2_0.settings):data[i].variableOt2_0.settings;
      data[i].fixedOt.length == 0?this.addFixedOut(data[i].fixedOt):data[i].fixedOt;
      data[i].restDays = this.readDays(data[i].restDays);
      data[i].workingDaysPerWeek = this.readDays(data[i].workingDaysPerWeek);
      data[i] = this.readWorkingHourPerDay(data[i]);
    }
    return data;
  }
  
  readDays(data){
    let dy =["mon","tue","wed","thu","fri","sat","sun"];
    let rtn=[];
    for (let s = 0; s < dy.length; s++) {
      let sd = false;
      for (let i = 0; i < data.length; i++) {
        if(data[i].substring(0,3)==dy[s]){
          rtn.push(data[i]);
          sd = true;
          break;
        }
      }
      if(!sd){rtn.push('');}
    }
    return rtn;
  }

  readWorkingHourPerDay(data){
    data.changeTimeSetting = {
      start:{
        time:"",
        ampm:"",
      },
      end:{
        time:"",
        ampm:"",
      }
    };

    if(data.workingHours.startTime != undefined){
      if(parseInt(data.workingHours.startTime ) < 1200){
        data.changeTimeSetting.start.time = data.workingHours.startTime;
        data.changeTimeSetting.start.ampm = "am";
      }if(parseInt(data.workingHours.startTime ) >= 1200){
        data.changeTimeSetting.time = (parseInt(data.workingHours.startTime)-1200).toString();
        data.changeTimeSetting.ampm = "pm";
      }

      data.changeTimeSetting.start.time = data.changeTimeSetting.start.time.length == 1?"000"+data.changeTimeSetting.start.time:data.changeTimeSetting.start.time; 
      data.changeTimeSetting.start.time = data.changeTimeSetting.start.time.length == 2?"00"+data.changeTimeSetting.start.time:data.changeTimeSetting.start.time; 
      data.changeTimeSetting.start.time = data.changeTimeSetting.start.time.length == 3?"0"+data.changeTimeSetting.start.time:data.changeTimeSetting.start.time; 
    }

    
    if(data.workingHours.endTime != undefined){
      if(parseInt(data.workingHours.endTime ) < 1200){
        data.changeTimeSetting.end.time = data.workingHours.endTime;
        data.changeTimeSetting.end.ampm = "am";
      }else{
        data.changeTimeSetting.end.time = (parseInt(data.workingHours.endTime)-1200).toString();
        data.changeTimeSetting.end.ampm = "pm";
      }
      data.changeTimeSetting.end.time = data.changeTimeSetting.end.time.length == 3?"0"+data.changeTimeSetting.end.time:data.changeTimeSetting.end.time; 
    }
    
    return data;
  }


  addmore(data){
    data.push(
      {
        "equality":"",
        "for": "",
        "numHours": ""
      }
    )
    return data;
  }

  addFixedOut(data){
    data.push({ 
      "active":false,
      "settings": {
      "additionalValue": "",
      "additionalValueUnit": "",
      "per": ""
    }});
    return data;
  }

  probationPeriodeTime =  [
    {name:"day(s)",value:"days"},
    {name:"month(s)",value:"months"},
    {name:"week(s)",value:"weeks"}
  ];
  fixedOtPer = [
    {name:"day",value:"day"},
    {name:"month",value:"month"},
    {name:"week",value:"week"}
  ];
  time=[
    {name:"00:00",value:"0000"},
    {name:"00:30",value:"0030"},
    {name:"01:00",value:"0100"},
    {name:"01:30",value:"0130"},
    {name:"02:00",value:"0200"},
    {name:"02:30",value:"0230"},
    {name:"03:00",value:"0300"},
    {name:"03:30",value:"0330"},
    {name:"04:00",value:"0400"},
    {name:"04:30",value:"0430"},
    {name:"05:00",value:"0500"},
    {name:"05:30",value:"0530"},
    {name:"06:00",value:"0600"},
    {name:"06:30",value:"0630"},
    {name:"07:00",value:"0700"},
    {name:"07:30",value:"0730"},
    {name:"08:00",value:"0800"},
    {name:"08:30",value:"0830"},
    {name:"09:00",value:"0900"},
    {name:"09:30",value:"0930"},
    {name:"10:00",value:"1000"},
    {name:"10:30",value:"1030"},
    {name:"11:00",value:"1100"},
    {name:"11:30",value:"1130"},
  ]
  ampm=["am","pm"];
  otTimeType=[
    {name:"Week Day",value:"weekday"},
    {name:"Weekend",value:"weekend"},
    {name:"Public Holiday",value:"ph"},
  ];
  hour=["1","2","3","4","5","6","7","8","9","10","11","12"];
  
  setTemplateParam(data){
    let rtn=[];
    for (let i = 0; i < data.length; i++) {
      let ss;
      let aa;
      let dd;
      let ch=[];
      let ah=[];
      let dh=[];

      for (let ssd = 0; ssd < data[i].variableOt1_0.settings.length; ssd++) {
        let d = this.objectDeleteNull(data[i].variableOt1_0.settings[ssd]);
        if(d.for != undefined && d.equality != undefined && d.numHours != undefined){
          ch.push(data[i].variableOt1_0.settings[ssd])
        }
      }
      
      ss={"active":data[i].variableOt1_0.active, "settings":ch};
      for (let ssd = 0; ssd < data[i].variableOt1_5.settings.length; ssd++) {
        let d = this.objectDeleteNull(data[i].variableOt1_5.settings[ssd]);
        if(d.for != undefined && d.equality != undefined && d.numHours != undefined){
          ah.push(data[i].variableOt1_5.settings[ssd])
        }
      }

      aa={"active":data[i].variableOt1_5.active, "settings":ah};

      for (let ssd = 0; ssd < data[i].variableOt2_0.settings.length; ssd++) {
        let d = this.objectDeleteNull(data[i].variableOt2_0.settings[ssd]);
        if(d.for != undefined && d.equality != undefined && d.numHours != undefined){
          dh.push(data[i].variableOt2_0.settings[ssd])
        }
      }

      dd={"active":data[i].variableOt2_0.active, "settings":dh};
      
      for (let id = 0; id < data[i].fixedOt.length; id++) {
        if(data[i].fixedOt[id].settings.additionalValue == "" || 
          data[i].fixedOt[id].settings.additionalValueUnit == "" ||
          data[i].fixedOt[id].settings.per == ""){
          data[i].fixedOt.splice(id,1);
        }
      }

      if(data[i].breakTimeFake == "other"){
        if(data[i].breakTimeOther == '' || data[i].breakTimeOther == null){
          data[i].breakTime=0;  
        }else{
          data[i].breakTime = parseInt(data[i].breakTimeOther)
        }
      }else{
        data[i].breakTime = parseInt(data[i].breakTimeFake)
      }

      let param:LooseObject = {
        completionBonusPayment : this.stringDelete(data[i].completionBonusPayment),
        probationPeriod: (data[i].probationPeriod.value == "" || data[i].probationPeriod.unit == "")?undefined:data[i].probationPeriod,
        workingDaysPerWeek: this.arrayDeleteNull(data[i].workingDaysPerWeek).length == 0? undefined: this.arrayDeleteNull(data[i].workingDaysPerWeek),
        workingHours: (this.objectDeleteNull(data[i].workingHours).startTime == undefined && this.objectDeleteNull(data[i].workingHours).endTime == undefined )?undefined:this.objectDeleteNull(data[i].workingHours),
        restDays: this.arrayDeleteNull(data[i].restDays).length == 0? undefined: this.arrayDeleteNull(data[i].restDays),
        variableOt1_0: ss,
        variableOt1_5: aa,
        variableOt2_0: dd,
        payslipFormat:this.stringDelete(data[i].payslipFormat),
        fixedOt: this.arrayDeleteNull(data[i].fixedOt),
        overtimePayEntitlement: data[i].overtimePayEntitlement == undefined?"":data[i].overtimePayEntitlement,
        templateName:data[i].templateName == undefined?"":data[i].templateName,
        breakTime:data[i].breakTime
      };
      rtn.push(param)
    }
    return rtn;
  }

  objectDeleteNull(data){
    const values = Object.values(data);
    const keys = Object.keys(data);

    for (let i = 0; i < values.length; i++) {
      if(values[i] == ""){
        delete data[keys[i]];
      }
    }
    return data;
  }

  stringDelete(data){
    let s = data;
    if(data == ""){s = undefined;}
    return s;
  }

  arrayDeleteNull(data){
    for (let i = 0; i < data.length; i++) {
      if(data[i] == ""){
        data.splice(i,1);
        i--;
      }
    }
    var filtered = data.filter(function (el) {
      return el != null;
    });
    return filtered;
  }

  addTemplate(data){
    let param = {
      templateName:"Template #"+(data.length+1),
      probationPeriod: 
        {
          value: "", 
          unit: ""
        }, 
      workingDaysPerWeek: ["","","","","","",""],
      workingHours: 
        {
          startTime: "", 
          endTime: ""
        },
      restDays:  ["","","","","","",""],
      variableOt1_0:
        {
          active: false, 
          settings: [
            {
              "equality":"",
              "for": "",
              "numHours": ""
            }
          ]
        },
      variableOt1_5:
        {
          active: false, 
          settings: [
            {
              "equality":"",
              "for": "",
              "numHours": ""
            }
          ]
        },
      variableOt2_0:
        {
          active: false, 
          settings: [
            {
              "equality":"",
              "for": "",
              "numHours": ""
            }
          ]
        },
      fixedOt:  
        [{
          active: false, 
          settings:
            {
              additionalValue: "",
              additionalValueUnit: "",
              per: ""
            }
        }],
      completionBonusPayment: "",
      payslipFormat: "",
      changeTimeSetting:{
        start:{
          time:"",
          ampm:"",
        },
        end:{
          time:"",
          ampm:"",
        }
      },
      overtimePayEntitlement:true,
    }
    return param;
  }

  deleteArray(data, idx){
    data.splice(idx, 1);
    return data;
  }

  getArrangement(data, type){
    let rtn = false;
    for (let i = 0; i < data.arrangement.length; i++) {
      if(data.arrangement[i].toLowerCase() == 'isa' && type.toLowerCase() == 'isa'){
        rtn =true;
        break;
      }
      if(data.arrangement[i].toLowerCase() == 'msa' && type.toLowerCase() == 'msa'){
        rtn =true;
        break;
      }      
    }
    return rtn;
  }

  setArrangement(data){
    let arr = [];
    if(data.arrangementIsa){
      arr.push('isa')
    }
    if(data.arrangementMsa){
      arr.push('msa')
    }
    return arr;
  }


  dataRowIsValid(data){
    let valid ={
      validation:false,
      desc:"",
      clnt_nm:false,
      clnt_rl:false,
      clnt_c_no:false,
      clnt_email:false,
    }
    if(data.clnt_nm  != '' && 
      data.clnt_rl  != '' && 
      data.clnt_c_no != '' && 
      data.clnt_email != '' ) {
        valid.validation = true;
        
        if(isNaN(data.clnt_c_no )){
          valid.validation = false;
          valid.desc = "Please fill contact number with number input";
          valid.clnt_c_no = true;
        }
        var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w+)+$/;
        if(!re.test(String(data.clnt_email).toLowerCase())){
          valid.validation = false;
          valid.desc = "Please fill up the email field with correct format (e.g. lorem@ipsum.com)";
          valid.clnt_email = true;
        }
      }else{
        valid.validation = false;
        valid.desc = "Please fill up all input box";
        valid.clnt_nm=data.clnt_nm  == ''?true:false;
        valid.clnt_rl=data.clnt_rl  == ''?true:false;
        valid.clnt_c_no=data.clnt_c_no  == ''?true:false;
        valid.clnt_email=data.clnt_email  == ''?true:false;
      }
      return valid;
  }

}

interface LooseObject {
  [key: string]: any
}
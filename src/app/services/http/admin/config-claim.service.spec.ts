import { TestBed } from '@angular/core/testing';

import { ConfigClaimService } from './config-claim.service';

describe('ConfigClaimService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConfigClaimService = TestBed.get(ConfigClaimService);
    expect(service).toBeTruthy();
  });
});

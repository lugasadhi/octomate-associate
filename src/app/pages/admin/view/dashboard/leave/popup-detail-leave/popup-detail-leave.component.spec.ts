import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupDetailLeaveComponent } from './popup-detail-leave.component';

describe('PopupDetailLeaveComponent', () => {
  let component: PopupDetailLeaveComponent;
  let fixture: ComponentFixture<PopupDetailLeaveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupDetailLeaveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupDetailLeaveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

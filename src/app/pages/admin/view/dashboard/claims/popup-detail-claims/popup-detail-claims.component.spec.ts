import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupDetailClaimsComponent } from './popup-detail-claims.component';

describe('PopupDetailClaimsComponent', () => {
  let component: PopupDetailClaimsComponent;
  let fixture: ComponentFixture<PopupDetailClaimsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupDetailClaimsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupDetailClaimsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

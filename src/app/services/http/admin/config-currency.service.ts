import { Injectable } from '@angular/core';
import {AccessService } from '../access.service';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ConfigCurrencyService {

  constructor(
    private _access:AccessService,
    private http:HttpClient,
  ) { }

  get(param){    
    let hdr = {
      headers : this._access.getHeader(),
      params: param 
    }
    return this.http.get(this._access.host()+"/configuration/currency", hdr);
  }
}

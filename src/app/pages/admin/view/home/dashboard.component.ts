import { Component, OnInit, OnDestroy } from '@angular/core';
import {GeneralService} from '../../../../services/general/general.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit,OnDestroy {

  panelOpenState = false;
  
  constructor(
    private general:GeneralService,
  ) { }

  ngOnInit() {
    $(".page-header").hide();
  }

  ngOnDestroy(){
    $(".page-header").show();
  }

}

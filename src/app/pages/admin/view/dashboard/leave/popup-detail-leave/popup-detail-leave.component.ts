import { Component, OnInit, Input, Output , EventEmitter, HostListener} from '@angular/core';
import {ClaimService} from '../../../../../../services/http/admin/claim.service';
import {GeneralService} from '../../../../../../services/general/general.service';


@Component({
  selector: 'popup-detail-leave',
  templateUrl: './popup-detail-leave.component.html',
  styleUrls: ['./popup-detail-leave.component.scss']
})
export class PopupDetailLeaveComponent implements OnInit {

  constructor(
    private _claim:ClaimService,
    private _general:GeneralService
  ) { }

  @HostListener('window:keyup', ['$event'])
  handleKeyboardEvent(event) { 
    let key = event.key;
    if(key == 'ArrowRight'){
      if(this.isDataNextAct){this.nextData();}
    }
    if(key == 'ArrowLeft'){
      if(this.isDataPrevAct){this.prevData();}
    }    
  }
  

  @Input('claimDtl') leaveDtl: anyObject;
  @Input('data') data: Array<anyObject>;
  @Output() close = new EventEmitter<boolean>();
  @Output() popUpPdf = new EventEmitter<anyObject>();
  @Output() reloadTable = new EventEmitter<boolean>();

  ngOnInit() {
    this.checkNextPrevActive(this.checkArrayNumber());   
    this.setEdit();
    console.log(this.leaveDtl);
  }
  
  setEdit(){
    this.editAmont = this.leaveDtl.receiptAmount;
    this.editRecieptDate = this.leaveDtl.receiptDate;
    this.editType  = this.leaveDtl.claimType;
  }

  disableEdit(){
    this.isEditAmount = false;
    this.isEditRecieptDate = false;
    this.isEditType = false;
  }

  isDataPrevAct = true;
  isDataNextAct = true;
  isEditAmount = false;
  isEditRecieptDate = false;
  isEditType = false;
  attchIdx = 0;

  cancel(){
    this.close.emit(false);
  }

  isPdfLoad = false;
  pdfLoad(render){
    this.isPdfLoad = render.total == undefined?true:false;
  }

  prevData(){
    let id = this.checkArrayNumber();
    this.checkNextPrevActive(id-1);
    let prevId = this.data[id-1];
    this.leaveDtl = prevId;
    this.clearing();
  }
  
  nextData(){
    let id = this.checkArrayNumber();
    this.checkNextPrevActive(id+1);
    let nextId = this.data[id+1];
    this.leaveDtl = nextId;
    this.clearing();
  }

  clearing(){
    this.setEdit();
    this.disableEdit();
    this.isRejectClicked = false;
  }

  checkNextPrevActive(idx){
    this.isDataPrevAct = idx==0?false:true;
    this.isDataNextAct = idx==this.data.length-1?false:true;
  }

  checkArrayNumber(){
    for (let i = 0; i < this.data.length; i++) {
      if(this.data[i]._id == this.leaveDtl._id){ 
        return i;
      }
    }
  }

  viewMoreAttch(lng){
    this.attchIdx = this.attchIdx <lng-1 ? this.attchIdx +1 :0;
  }

  showPopUpPDF(source){
    this.popUpPdf.emit({
      show:true,
      source:source
    });
  }

  rejectRemarks = '';
  isRejectClicked=false;
  rejectClaim(){
    if(this.isRejectClicked){
      this._claim.reject(this.leaveDtl.claimsGroupId, this.rejectRemarks).subscribe(
        (resp)=>{
          console.log(resp);
          this.reloadTable.emit(true);
          this.cancel();
        },(err)=>{
          console.log(err);
        }
      );
    }
    this.isRejectClicked = true;
  }

  verify(){
    this._claim.approve(this.leaveDtl.claimsGroupId).subscribe(
      (resp)=>{
        console.log(resp);
        this.reloadTable.emit(true);
        this.cancel();
      },(err)=>{
        console.log(err);
      }
    );
  }

  editAmont:number;
  editRecieptDate:string;
  editType:string;
  saveEditAmount(){
    let param ={receiptAmount:this.editAmont};
    this.editClaim(param);
    this.isEditAmount = false;
  }

  saveEditRecieptDate(){
    let param ={receiptDate:this.editRecieptDate};
    this.editClaim(param);
    this.isEditRecieptDate = false;
  }

  saveEditType(){
    let param ={claimType:this.editType};
    this.editClaim(param);
    this.isEditType = false;
  }

   editClaim(param){
    this._general.setLoading(true);
    let prm = {...param, claimId: this.leaveDtl._id};
    this._claim.put(prm).subscribe((res)=>{
      this._general.setLoading(false);
      this.reloadTable.emit(true);
      this.leaveDtl = res;
    },(err)=>{
      this._general.setLoading(false);
      console.log(err);
    })
  }

}


interface anyObject {
  [key: string]: any
}
import { Component, OnInit, ViewChild } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {Router} from '@angular/router';
import {GeneralService} from '../../../../../services/general/general.service';
import {ClientService} from '../../../../../services/http/admin/client.service';
import { PageEvent } from '@angular/material';


@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss'],
  styles: [`
    ngb-popover-window{
      background:#000;
    }
  `]
})

export class ClientComponent implements OnInit {
  allClient;
  sclient;
  dclient;
  search='';

  constructor(
    private route:Router,
    private general:GeneralService,
    private client:ClientService,
  ) { }

  displayedColumns = ['_id', 'clientName', 'isDraft', 'createdByName', 'lastModifiedTime','action'];
  dataSource;
  pageLength = 0;

  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this.getClient('',0, this.pageSizeOptions[0]);
  }
  
  myVar;
  applyFilter(filterValue: string, key) {
    clearTimeout(this.myVar);
    this.search = filterValue.trim().toLowerCase();
    if(key == 'Enter'){
      this.getClient('',0, this.pageSizeOptions[0]);
    }else{
      this.myVar = setTimeout(() => { 
       this.getClient('',0, this.pageSizeOptions[0]);
    }, 2000);
    }
  }


  sortOrder;
  sortBy;
  sortData(data){
    this.sortOrder= data.direction;
    this.sortBy=this.sortOrder == ''?'':data.active;
    this.getClient('',0, this.pageSizeOptions[0]);
  }

  restartTable(data){
    this.dataSource = new MatTableDataSource<clientStatus>(data);
    this.dataSource.sort = this.sort;
  }
  
  goto(data){
    if(data.isDraft){
      this.route.navigate(['/admin/task/client/add-new-client'],{queryParams: {id:data._id}});
    }else if(!data.isDraft){
      this.route.navigate(['/admin/task/client/client-ID'],{queryParams: {id:data._id}});
    }
  }


  getClient(id,page, itemPerPage){
    this.general.setLoading(true);
    this.client.get(id,page, itemPerPage,this.search).subscribe(
      (resp:anyObject)=>{
        this.general.setLoading(false);
        this.sclient = resp.clientList;
        this.pageLength = resp.totalNumClients;
        this.restartTable(resp.clientList);
      },(err)=>{
        this.general.setLoading(false);
      }
    );
  }
    
  deleteConfirm ={
    show:false,
    data:{}
  }
  deleteClient(data){
    this.general.setLoading(true);
    let pgs = this.pageOpt == undefined?this.pageSizeOptions[0]:this.pageOpt.pageSize;
    if(data.isDraft){
      this.client.deleteDraft(data._id).subscribe((resp)=>{
        this.general.setLoading(false);
        this.getClient('',0, pgs);
        this.deleteConfirm.show = false;
      });
    }else {
      this.client.delete(data._id).subscribe((resp)=>{
        this.general.setLoading(false);
        this.getClient('',0, pgs);
        this.deleteConfirm.show = false;
      });
    }
  }

  pageSizeOptions: number[] = [ 15, 50, 100];

  setPageSizeOptions(setPageSizeOptionsInput: string) {
    this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
  }

  pageOpt;
  pageEvent(data){
    this.pageOpt = data;
    this.getClient('',data.pageIndex, data.pageSize);
  };



}

interface anyObject {
  [key: string]: any
}

interface clientStatus {
  clientId: string;
  clientId6: string;
  clientName: string;
  status: string;
  created_by: string;
  date_modifield: string,
}

interface userDt{
  adminId: String,
  email: String,
  name: String,
  phoneNumber: String,
  role: String,
}
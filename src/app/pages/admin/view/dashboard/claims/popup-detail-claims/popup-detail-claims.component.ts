import { Component, OnInit, Input, Output , EventEmitter, HostListener} from '@angular/core';
import {ClaimService} from '../../../../../../services/http/admin/claim.service';
import {GeneralService} from '../../../../../../services/general/general.service';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';

import * as _moment from 'moment';
const moment =  _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'DD/MM/YYYY',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};


@Component({
  selector: 'claim-popup-detail-claims',
  templateUrl: './popup-detail-claims.component.html',
  styleUrls: ['./popup-detail-claims.component.scss'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class PopupDetailClaimsComponent implements OnInit {

  constructor(
    private _claim:ClaimService,
    private _general:GeneralService
  ) { }

  @HostListener('window:keyup', ['$event'])
  handleKeyboardEvent(event) { 
    let key = event.key;
    if(key == 'ArrowRight'){
      if(this.isDataNextAct){this.nextData();}
    }
    if(key == 'ArrowLeft'){
      if(this.isDataPrevAct){this.prevData();}
    }    
  }
  

  @Input('claimDtl') claimDtl: anyObject;
  @Input('data') data: Array<anyObject>;
  @Output() close = new EventEmitter<boolean>();
  @Output() popUpPdf = new EventEmitter<anyObject>();
  @Output() reloadTable = new EventEmitter<boolean>();

  ngOnInit() {
    this.checkNextPrevActive(this.checkArrayNumber());   
    this.setEdit();
    console.log(this.claimDtl);
    
  }
  
  setEdit(){
    this.editAmont = this.claimDtl.receiptAmount;
    this.editRecieptDate = new Date(this.claimDtl.receiptDate);
    this.editType  = this.claimDtl.claimType;
  }

  disableEdit(){
    this.isEditAmount = false;
    this.isEditRecieptDate = false;
    this.isEditType = false;
  }

  isDataPrevAct = true;
  isDataNextAct = true;
  isEditAmount = false;
  isEditRecieptDate = false;
  isEditType = false;
  attchIdx = 0;

  cancel(){
    this.close.emit(false);
  }

  isPdfLoad = false;
  pdfLoad(render){
    this.isPdfLoad = render.total == undefined?true:false;
  }

  prevData(){
    let id = this.checkArrayNumber();
    this.checkNextPrevActive(id-1);
    let prevId = this.data[id-1];
    this.claimDtl = prevId;
    this.clearing();
  }
  
  nextData(){
    let id = this.checkArrayNumber();
    this.checkNextPrevActive(id+1);
    let nextId = this.data[id+1];
    this.claimDtl = nextId;
    this.clearing();
  }

  clearing(){
    this.setEdit();
    this.disableEdit();
    this.isRejectClicked = false;
  }

  checkNextPrevActive(idx){
    this.isDataPrevAct = idx==0?false:true;
    this.isDataNextAct = idx==this.data.length-1?false:true;
  }

  checkArrayNumber(){
    for (let i = 0; i < this.data.length; i++) {
      if(this.data[i]._id == this.claimDtl._id){ 
        return i;
      }
    }
  }

  viewMoreAttch(lng){
    this.attchIdx = this.attchIdx <lng-1 ? this.attchIdx +1 :0;
  }

  showPopUpPDF(source){
    this.popUpPdf.emit({
      show:true,
      source:source
    });
  }

  rejectRemarks = '';
  isRejectClicked=false;
  rejectClaim(){
    if(this.isRejectClicked){
      this._claim.reject(this.claimDtl.claimsGroupId, this.rejectRemarks).subscribe(
        (resp)=>{
          console.log(resp);
          this.reloadTable.emit(true);
          this.cancel();
        },(err)=>{
          console.log(err);
        }
      );
    }
    this.isRejectClicked = true;
  }

  verify(){
    this._claim.approve(this.claimDtl.claimsGroupId).subscribe(
      (resp)=>{
        console.log(resp);
        this.reloadTable.emit(true);
        this.cancel();
      },(err)=>{
        console.log(err);
      }
    );
  }

  editAmont:number;
  editRecieptDate:Date;
  editType:string;
  saveEditAmount(){
    let param ={receiptAmount:this.editAmont};
    this.editClaim(param);
    this.isEditAmount = false;
  }

  saveEditRecieptDate(){
    let param ={receiptDate:this.editRecieptDate};
    this.editClaim(param);
    this.isEditRecieptDate = false;
  }

  saveEditType(){
    let param ={claimType:this.editType};
    this.editClaim(param);
    this.isEditType = false;
  }

   editClaim(param){
    this._general.setLoading(true);
    let prm = {...param, claimId: this.claimDtl._id};
    this._claim.put(prm).subscribe((res)=>{
      this._general.setLoading(false);
      this.reloadTable.emit(true);
      this.claimDtl = res;
    },(err)=>{
      this._general.setLoading(false);
      console.log(err);
    })
  }

  isImage(data){
    data = data.split(".");
    console.log(data);
    if(data[data.length-1] == 'pdf'){
      return false;
    }else{
      return true;
    }
  }

}


interface anyObject {
  [key: string]: any
}
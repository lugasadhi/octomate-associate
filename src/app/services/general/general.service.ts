import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js'; 
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GeneralService {

  encPassword = "nikahenak";

  constructor(
    
  ) { }

  setCookies(cname, cvalue, exhour ){
    var d = new Date();
    d.setTime(d.getTime() + (exhour*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }

  getCookies(cname){
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        let rslt =  c.substring(name.length, c.length);
        return rslt;
      }
    }
    return "";
  }

  checkCookie(cname){
    var dt = this.getCookies(cname);
    if (dt != "") {
     return true;
    } else {
      return false;
    }
  }

  deleteCookie(cname){
    if(this.checkCookie(cname)){
      this.setCookies(cname,'',-1);
    }
  }


  encrypt(data){
    let result = CryptoJS.AES.encrypt(data.trim(), this.encPassword.trim()).toString();  
    return result;
  }

  decrypt(data){
    let result;
    if(data == null || data == undefined){
      result = data;
    }else{
      result = CryptoJS.AES.decrypt(data.trim(), this.encPassword.trim()).toString(CryptoJS.enc.Utf8);;
    }
    return result;
  }

  setLocalStorage(name,data){
    let dt2 = JSON.stringify(data);
    window.localStorage.setItem(name,this.encrypt(dt2));
  }

  getLocalStorage(name){
     let dt = this.decrypt(window.localStorage.getItem(name));
     return JSON.parse(dt);
  }

  deleteLocalStorage(name){
    window.localStorage.removeItem(name);
  }

  setSessionStorage(name,data){
    let dt2 = JSON.stringify(data);
    window.sessionStorage.setItem(name,this.encrypt(dt2));
  }

  getSessionStorage(name){
      let dt = this.decrypt(window.sessionStorage.getItem(name));
      return JSON.parse(dt);
  }

  deleteSessionStorage(name){
    window.sessionStorage.removeItem(name);
  }
  
  getToken(){
    if(this.getCookies("token") != '' && this.getCookies("token") != undefined && this.getCookies("token") != null){
      return this.getCookies("token")
    }else{
      return this.getSessionStorage("token")!= undefined ? this.getSessionStorage("token"): '';
    }
  }

  private loading = new BehaviorSubject<boolean>(false);
  cast = this.loading.asObservable();
  setLoading(bool){
    this.loading.next(bool);
  }

  
}

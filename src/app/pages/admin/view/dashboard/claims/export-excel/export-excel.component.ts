import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'claim-export-excel',
  templateUrl: './export-excel.component.html',
  styleUrls: ['./export-excel.component.scss']
})
export class ExportExcelComponent implements OnInit {

  constructor() { }

  @Input('dataTable') dataTable: Array<object>;
  @Input('titleTable') titleTable: Array<string>;

  ngOnInit() {
  }

}

import { Component, OnInit } from '@angular/core';
import {FormControl, Validators, FormGroup} from '@angular/forms';
import { Router, ActivatedRoute  } from '@angular/router';

import {AuthService} from '../../../../services/http/auth/auth.service';
import {GeneralService} from '../../../../services/general/general.service';


@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss','../../general.scss']
})


export class ForgotPasswordComponent implements OnInit {
  forgetPassForm;
  invalidEmail = false;
  actionCode;
  continueUrl;
  lang;
  formActive =true;

  constructor(
    private auth:AuthService,
    private router:Router,
    private route:ActivatedRoute,
    private general:GeneralService,
  ) { }

  ngOnInit() {
    

    this.forgetPassForm = new FormGroup({
      email : new FormControl('', [
        Validators.required,
        Validators.email,
      ])
    })
  }

  deleteEmailInvalidErr(){
    this.invalidEmail = false;
  }

  onSubmit(){
    if(this.forgetPassForm.valid){
      this.general.setLoading(true);
      this.auth.forgetPassword(this.forgetPassForm.value.email).subscribe(
        (res)=>{
          this.general.setLoading(false);
          this.formActive = false;
        },(err)=>{
          this.general.setLoading(false);
          if(err.error == "Admin not found!"){
            this.invalidEmail = true;
          }
        }
      )
    }
  }
}

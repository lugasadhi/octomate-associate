import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-new-edit-claim',
  templateUrl: './new-edit-claim.component.html',
  styleUrls: ['./new-edit-claim.component.scss']
})
export class NewEditClaimComponent implements OnInit {

  constructor(
    private _actRoute:ActivatedRoute,
  ) { 
    this._actRoute.queryParams
    .subscribe(params => {
      this.queryParam = params;
    });

    this._actRoute.url.subscribe(
      (url)=>{
        this.path = url[url.length-1].path ;
        if(url[url.length-1].path == 'claim-ID'){
          this.isEdit = true;
          this.disableForm();
        }else{
          this.isEdit = false;
          this.isDisable = false;
          this.formConfig();
        }
      }
    )
  }

  path;
  claimConfig;
  queryParam;
  subClaimInpt=[""];
  actionList=["all employees by default","select employees manually"];
  isEdit;
  isDisable;

  ngOnInit() {
  }

  formConfig(){
    this.claimConfig = new FormGroup({
      claimType: new FormControl(),
      subclaim : new FormControl(false),
      description : new FormControl(false),
      action : new FormControl(),
    }); 
  }

  disableForm(){
    this.isDisable = true;
    this.formConfig();
    this.claimConfig.disable();
  }

  enableForm(){
    this.isDisable = false;
    this.claimConfig.enable();
    if(this.path == "claim-ID"){
      this.claimConfig.controls['_id'].disable();
    }
  }

  ngForFromInt(lg){
    let rtn = [];
    for (let i = 0; i < lg; i++) {
      rtn = [...rtn, ''];
    }
    return rtn;
  }

  onSubmit(){

  }

  edit(){
    this.enableForm()
  }

  addDocument(){
    this.subClaimInpt.push(null);
  }

  removeDocument(idx){
    this.subClaimInpt.splice(idx,1);
    if(this.subClaimInpt.length == 0){
      this.subClaimInpt = [...this.subClaimInpt, ''];
      this.claimConfig.value.subclaim = false;
    }
  }

}

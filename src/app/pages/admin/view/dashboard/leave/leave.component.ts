import { Component, OnInit ,ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import { ExportAsService, ExportAsConfig } from 'ngx-export-as';
import {Router, ActivatedRoute} from '@angular/router';

import {LeaveService} from '../../../../../services/http/admin/leave.service';
import {GeneralService} from '../../../../../services/general/general.service';


@Component({
  selector: 'app-leave',
  templateUrl: './leave.component.html',
  styleUrls: ['./leave.component.scss']
})
export class LeaveComponent implements OnInit {

  displayedColumns = ['associateId', 'createdByName', 'associateNric', 'leaveType', 'startDate', 'endDate','duration','status','docUrls'];
  dataSource;
  
  @ViewChild(MatSort) sort: MatSort;

  claimDtl;
  popupDetail = false;
  isFilter = false;
  isRejectClicked = false;
  data;
  pageLength=0;
  filterText='';
  filter='';
  myVar;
  sortBy ='';
  sortOrder = '';
  dataExportExcel;
  dataList;



  applyFilter(filterValue: string, key) {
    clearTimeout(this.myVar);
    this.filterText = filterValue.trim().toLowerCase();
    if(key == 'Enter'){
      this.http_getClaimList(0);
    }else{
      this.myVar = setTimeout(() => { 
        this.http_getClaimList(0);
      }, 2000);
    }
  }

  setQueryParameter(searchText, filter){
    let param ={
      search:searchText,
      filter:filter
    }
    this.router.navigate([], { queryParams: param });
  }

  getQueryParam(){
    this.actRouter.queryParams.subscribe(params => {
      console.log(params );
    });
  }

  exportAsConfig: ExportAsConfig = {
    type: 'xls', // the type you want to download
    elementId: 'exportTable', // the id of html/table element
  }

  constructor(
    private exportAsService: ExportAsService,
    private _httpLeave: LeaveService,
    private _general:GeneralService,
    private router:Router,
    private actRouter:ActivatedRoute
  ) { }

  ngOnInit() {
    this.http_getClaimList(0);
    this.actRouter
  }

  http_getClaimList(page){
    this._general.setLoading(true);
    let param ={
      page:page,
      numLeavesPerPage:  this.pageOpt != undefined? this.pageOpt.pageSize:this.pageSizeOptions[0],
      searchString :this.filterText,
      sortBy:this.sortBy,
      sortOrder:this.sortOrder,
      queryString:this.filterQuery()
    };
    this._httpLeave.get(param).subscribe(
      (resp:anyObject)=>{
        console.log(resp);
        this._general.setLoading(false);
        this.pageLength = resp.totalNumLeaves;
        this.data = resp.leaveList;
        this.dataSource = new MatTableDataSource(this.data);
        this.dataList = resp;
      },(err)=>{
        this._general.setLoading(false);
        console.log(err);
      }
    )
  }

  sortData(data){
    this.sortOrder= data.direction;
    this.sortBy=this.sortOrder == ''?'':data.active;
    this.http_getClaimList(0);
  }

  pageSizeOptions: number[] = [ 15, 50, 100];

  pageOpt;
  pageEvent(data){
    this.pageOpt = data;
    this.http_getClaimList(data.pageIndex );
  };

  isDataPrevAct = true;
  isDataNextAct = true;
  showClaimDetail(data){
    this.popupDetail = true;
    this.claimDtl = data;
  }

  prevData(){
    let id = this.checkArrayNumber();
    this.checkNextPrevActive(id-1);
    let prevId = this.data[id-1];
    this.claimDtl = prevId;
  }
  
  nextData(){
    let id = this.checkArrayNumber();
    this.checkNextPrevActive(id+1);
    let nextId = this.data[id+1];
    this.claimDtl = nextId;
  }

  checkNextPrevActive(idx){
    this.isDataPrevAct = idx==0?false:true;
    this.isDataNextAct = idx==this.data.length-1?false:true;
  }

  checkArrayNumber(){
    for (let i = 0; i < this.data.length; i++) {
      if(this.data[i].associateId == this.claimDtl.associateId){ 
        return i;
      }
    }
  }

  filterData(){
    this.isFilter = !this.isFilter;
  }

  isShowFilter={
    status:false,
    show:-1,
    isOutside: false
  }

  showFilter(data){
    this.isShowFilter.status = this.isShowFilter.show==data? !this.isShowFilter.status : true;
    this.isShowFilter.show = data;
    this.isShowFilter.isOutside = false;
  }

  cancelFilter(data:boolean){
    this.isShowFilter.status = data;
  }

  filterByassociateId=[];
  filterBycreatedByName=[];
  filterByNric=[];
  filterByLeaveTypes=[];
  filterByStatus=[];
  filterByAttachment=[];
  filterByStartDate=[];
  filterByEndDate=[];
  filterByDuration=[];

  applyFilterData(data, type){
    if(type == 'associateId'){
      this.filterByassociateId = data;
    }
    if(type =='createdByName'){
      this.filterBycreatedByName = data;
    }
    if(type =='associateNric'){
      this.filterByNric = data;
    }
    if(type=='leaveType'){
      this.filterByLeaveTypes =data;
    }
    if(type=='startDate'){
      this.filterByStartDate =data;
    }
    if(type=='endDate'){
      this.filterByEndDate =data;
    }
    if(type=='duration'){
      this.filterByDuration =data;
    }
    if(type=='status'){
      this.filterByStatus =data;
    }
    if(type=='attachment'){
      this.filterByAttachment =data;
    }

    this.http_getClaimList(0);
  }
  

  filterQuery(){
    let asc =  this.filterByassociateId.length > 0?`associateId:${this.filterByassociateId.toString()};`:'';
    let name = this.filterBycreatedByName.length>0?`createdByName:${this.filterBycreatedByName.toString()};`:"";
    let nric = this.filterByNric.length>0?`associateNric:${this.filterByNric.toString()};`:"";
    let leaveType = this.filterByLeaveTypes.length>0?`leaveType:${this.filterByLeaveTypes.toString()};`:"";
    let startDate = this.filterByStartDate.length>0?`startDate:${this.filterByStartDate.toString()};`:"";
    let endDate = this.filterByEndDate.length>0?`endDate:${this.filterByEndDate.toString()};`:"";
    let status = this.filterByStatus.length>0?`status:${this.filterByStatus.toString()};`:"";
    let attachment = this.filterByAttachment.length>0?`attachment:${this.filterByAttachment.toString()};`:"";
    
    let res = asc+name+nric+leaveType+startDate+endDate+status+attachment;
    return res;    
  }


  export() {
    this._general.setLoading(true);
    let param ={
      queryString:this.filterQuery()
    };
    this._httpLeave.get(param).subscribe(
      (resp:anyObject)=>{
        console.log(resp);
        this.dataExportExcel = resp.leaveList;
        setTimeout(()=> {this.exportExcel();}, 3000);
      },(err)=>{
        this._general.setLoading(false);
        console.log(err);
      }
    )
  }

  exportExcel(){
    this.exportAsService.save(this.exportAsConfig, 'Leaves List').subscribe((ss) => {
      this._general.setLoading(false);
      console.log(ss);
    });
  }

  popupDetPdf={
    show:false,
    source:''
  }

  setDuration(duration){
    let rtn;
    
    if(duration <= 7){
      rtn = duration+" day(s)";
    }else{
      rtn = `${Math.floor(duration/7)} week(s)<br> ${duration%7} day(s) `;
    }
    return rtn;
  }

}


interface anyObject {
  [key: string]: any
}

import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'leave-export-excel',
  templateUrl: './export-excel.component.html',
  styleUrls: ['./export-excel.component.scss']
})
export class LeaveExportExcelComponent implements OnInit {

  constructor() { }

  @Input('dataTable') dataTable: Array<object>;
  @Input('titleTable') titleTable: Array<string>;

  ngOnInit() {
  }

}

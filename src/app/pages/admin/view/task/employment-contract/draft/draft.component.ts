import { Component, OnInit, ViewChild, Output, Input, EventEmitter } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';


@Component({
  selector: 'app-emp-cont-draft',
  templateUrl: './draft.component.html',
  styleUrls: ['./draft.component.scss']
})
export class DraftComponent implements OnInit {

  displayedColumns = ['client_name', 'job_order_name', 'associate_id', 'associate_name', 'attacment','action'];
  dataSource;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  
  @Input('draftData') data: Array<dataInf>;

  constructor() { }

  ngOnInit() {
    this.dataSource = new MatTableDataSource<dataInf>(this.data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  rowEdit = false;

  tabelRowId;
  rowClick(rowId){
    if( this.tabelRowId != rowId.contractId){this.rowEdit = false}
    this.tabelRowId = rowId.contractId;
  }

  editRow(rowData){
    this.rowEdit =true;
  }

  deleteRow(){

  }

  doneRow(){
    this.rowEdit =false;
  }

}



interface dataInf{
  'client_name':string, 
  'job_order_name':string, 
  'associate_id':string, 
  'associate_name':string, 
  'attacment':string,
  'contractId':string,
}
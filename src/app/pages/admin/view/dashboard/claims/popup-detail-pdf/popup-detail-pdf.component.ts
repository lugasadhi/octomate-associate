import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'popup-detail-pdf',
  templateUrl: './popup-detail-pdf.component.html',
  styleUrls: ['./popup-detail-pdf.component.scss']
})
export class PopupDetailPdfComponent implements OnInit {

  constructor() { }
  @Input('source') source: string;
  @Output() close = new EventEmitter<boolean>();


  ngOnInit() {
  }

  isPdfLoad = false;
  pdfLoad(render){
    this.isPdfLoad = render.total == undefined?true:false;
  }

  closePopup(){
    this.close.emit(false);
  }
}

import { Component, OnInit } from '@angular/core';
import {FormControl, Validators, FormGroup} from '@angular/forms';
import {  ActivatedRoute  } from '@angular/router';
import {AuthService} from '../../../../services/http/auth/auth.service';
import {GeneralService} from '../../../../services/general/general.service';

@Component({
  selector: 'app-new-password',
  templateUrl: './new-password.component.html',
  styleUrls: ['./new-password.component.scss','../../general.scss']
})
export class NewPasswordComponent implements OnInit {
  token;
  newPasswordFrom;
  formActive=true;
  passMatches= true;

  passChangeTtl;
  passChangeDesc;
  backToLogin:boolean;
  
  constructor(
    private route:ActivatedRoute,
    private auth:AuthService,
    private general:GeneralService,
  ) { }


  ngOnInit() {
    this.token = this.route.snapshot.paramMap.get("token");

    this.newPasswordFrom = new FormGroup({
      newPassword : new FormControl('', [
        Validators.required,
      ]),
      confirmPassword : new FormControl('', [
        Validators.required,
      ]),
    })

  }

  
  //button function
  onSubmit(){
    if(this.newPasswordFrom.valid){
      this.passMatches = this.newPasswordFrom.value.newPassword == this.newPasswordFrom.value.confirmPassword;
      if(this.passMatches){
        this.changePasswordAct(this.newPasswordFrom.value.newPassword, this.newPasswordFrom.value.confirmPassword);
      }
    }
  }

  changePasswordAct(newPass, confirmPass){
    this.general.setLoading(true);
    this.auth.resetPassword(newPass, confirmPass, this.token).subscribe(
      (res)=>{
        this.general.setLoading(false);
        this.formActive = false;
        this.passChangeTtl = "Password Changed";
        this.passChangeDesc = "Your password has been changed successfully ";
        this.backToLogin = true;
      },(err)=>{
        this.general.setLoading(false);
        this.formActive = false;
        this.backToLogin = false;
        this.passChangeTtl = "Error";
        if(err.error == "Token is invalied, or has expired. Try again."){
          this.passChangeDesc = err.error;
        }
      }
    )
  }


}

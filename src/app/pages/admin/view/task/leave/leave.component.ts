import { Component, OnInit } from '@angular/core';
import { ConfigLeaveService } from 'src/app/services/http/admin/config-leave.service'
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-leave',
  templateUrl: './leave.component.html',
  styleUrls: ['./leave.component.scss']
})
export class LeaveComponent implements OnInit {

  constructor(
    private http_configLeave:ConfigLeaveService
  ) { }

  dataSource;
  displayedColumns=['_id','leaveType','statutory','unpaid','action','document','remarks','delete'];
  dataTable=[
    {
      _id       :'0123018239081203',
      leaveType :'leaveType',           
      statutory : true,         
      unpaid    : false,      
      action    :'action',      
      document  : false,       
      remark    : true,      
    }
  ];

  ngOnInit() {
    this.getConfigLeave();
  }

  getConfigLeave(){
    let param = {
      page :0,
      numLeaveConfigurationsPerPage:15
    }
    this.http_configLeave.get(param).subscribe(
      (resp:anyObject)=>{
        console.log(resp);
        this.dataTable = resp.leaveConfigurationList;
      }
    )
  }

  restartTable(){
    this.dataSource = new MatTableDataSource(this.dataTable);
  }
  
  applyFilter(filterValue: string, key) {
  
  }

  sortData(data){

  }



}

interface anyObject {
  [key: string]: any
}
import { Injectable } from '@angular/core';
import {AccessService } from '../access.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HistoryService {

  constructor(
    private _access:AccessService,
    private http:HttpClient,

  ) { }

  get(param){    
    let hdr = {
      headers : this._access.getHeader(),
      params: param 
    }
    return this.http.get<historyInf>(this._access.host()+"/history", hdr);
  }
}

interface historyInf{
  blockHeight:number,
  historyData:Array<historyDetail>
  maxPageNum:number,
  latestBlock:historyDetail,
}

interface historyDetail{
  action: String,
  firstPartyId: String,
  firstPartyName: String,
  firstPartyRole:  String,
  secondPartyId:  String,
  secondPartyName:  String,
  secondPartyRole:  String,
  timestamp: Date,
  __v: number,
  _id:  String
}
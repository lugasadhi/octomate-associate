import { Component, OnInit, ViewChild } from '@angular/core';
import {FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import {MatHorizontalStepper} from '@angular/material/stepper';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {ClientService} from '../../../../../../services/http/admin/client.service';
import {GeneralService} from '../../../../../../services/general/general.service';
import {ClientmanagerService} from '../../../../../../services/http/admin/clientmanager.service'
import { ActivatedRoute,Router } from '@angular/router';
import {ClientServiceService} from '../client-service.service';
import * as $ from 'jquery';


@Component({
  selector: 'app-add-client',
  templateUrl: './add-client.component.html',
  styleUrls: ['./add-client.component.scss']
})
export class AddClientComponent implements OnInit {
  paramId;
  isLinear = true;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;

  constructor(
    private _formBuilder: FormBuilder,
    private _client:ClientService,
    private _general:GeneralService,
    private _actRoute:ActivatedRoute,
    private _clientManager:ClientmanagerService,
    private _generalClient:ClientServiceService,
    private router:Router,
  ) { }

  @ViewChild(MatHorizontalStepper) stepper: MatHorizontalStepper;
  
  complete(){
    this.stepper.selected.completed = true;
    this.stepper.selected.editable = true;
    this.stepper.next();
  }


  // set users
  displayedColumns = ['name', 'role', 'contact_no', 'email', 'action'];
  dataSource;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      clientUEN: ['', Validators.required,],
      clientName: ['', Validators.required,],
      officeAddress1:[''],
      officeAddress2:[''],
      arrangementMsa:[false,],
      arrangementIsa:[false,]
    });


    //configure master templates
    this.days = this._generalClient.setDays(this.days);

    //check query parameter
    this._actRoute.queryParams
      .subscribe(params => {
        this.paramId = params.id; 
        if(this.paramId != undefined){
          this.setForm();
        }else{
          this.setForm1();
        }
      });
  }


  setForm(){
    this._general.setLoading(true);
    this._client.getDraft(this.paramId,'').subscribe(
      (resp:LooseObject)=>{
        this._general.setLoading(false);
        this.firstFormGroup.setValue({
          clientUEN:resp.wowId,
          clientName:resp.clientName,
          officeAddress1:resp.officeAddressLine1,
          officeAddress2:resp.officeAddressLine2,
          arrangementIsa:this._generalClient.getArrangement(resp,'isa'),
          arrangementMsa:this._generalClient.getArrangement(resp,'msa'),
        });
        this.templates = this._generalClient.setParam(resp.templates);
        this.templates = this._generalClient.setTemplate(this.templates);
        this.activeTemplate = this.templates.length > 0?1:0;
        if(this.activeTemplate>0){
          this.templateChange(this.activeTemplate-1);
        }
        this.getClientManager(resp.clientManagers);
        console.log(resp);
        
        
        let title = "Client ID: "+resp.clientName +" (#"+ this.paramId.substring(this.paramId.length-6, this.paramId.length).toUpperCase()+")";
        $('.pageheader-title').hide();
        $( "<h2 class='pageheader-title ssdss'></h2>" ).insertBefore( ".pageheader-title" );
        $(".ssdss").css({
          "font-family": "Roboto",
          "font-size": "24px",
          "line-height": "1.5",
          "color": "#5cb8b2",
          "font-weight": "normal",
          "text-transform": "capitalize",
          "margin-bottom": "4px",
        })
        $(".ssdss, .breadcrumb-item:last-child span").html(title);

      },(err)=>{
        console.log(err);
      }
    )
  }
 
  setForm1(){
    this.firstFormGroup.setValue({
      clientUEN:"",
      clientName:"",
      officeAddress1:"",
      officeAddress2:"",
      arrangementMsa:false,
      arrangementIsa:false,
    });
  }

  ngOnDestroy(): void {
    $('.pageheader-title').show();
    $('.ssdss').remove();
  }


  //========================== step 1 ========================== 
  



  //========================== end of step 1 ==========================


  //==========================step 2==========================
  tblEdit=false;
  deletePopup = false;
  selectedRowIndex;

  clnt_nm;clnt_rl;clnt_c_no;clnt_email;

  chck(userdata){
    if(!this.isNewUserCreate){
      if(this.selectedRowIndex != userdata.ids){
        this.tblEdit = false;
      }
      this.selectedRowIndex = userdata.ids;
    }
  }

  isNewUserCreate= false;
  
  newUser(){
    if(!this.isNewUserCreate){
      this.isNewUserCreate = true;
      this.tblEdit = true;

      if(this.clientMng == undefined){
        this.clientMng = [];
      }

      let sd = {
        ids:(Math.floor(Math.random() * Math.floor(1000))).toString(),
        clientName: '',
        clientManagerName:'',
        jobTitle: '',
        phoneNumber: '',
        email: '',
        draftId:''
      };

      this.clientMng = [sd, ...this.clientMng];
      
      this.selectedRowIndex = this.clientMng[0].ids;
      this.clnt_nm='';
      this.clnt_rl= '';
      this.clnt_c_no ='';
      this.clnt_email ='';

      this.dataSource = new MatTableDataSource<clientManagerIterface>(this.clientMng);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
  }

  
  editRow(data){
    this.tblEdit = true;
    this.clnt_nm = data.clientManagerName;
    this.clnt_rl = data.jobTitle;
    this.clnt_c_no = data.phoneNumber;
    this.clnt_email = data.email;

  }

  deleteIds;
  deleteRow(data){
    this.deleteIds = data.ids;
    this.deletePopup = true;
  }

  onKeydown(event, data){
    if (event.key === "Enter") {
      this.doneRow(data);
    }
  }

  newClienManager=[];
  errClient;
  doneRow(data){
    this.errClient = this._generalClient.dataRowIsValid({
      clnt_nm: this.clnt_nm,
      clnt_rl: this.clnt_rl,
      clnt_c_no: this.clnt_c_no,
      clnt_email: this.clnt_email,
    });
    
    if(this.errClient["validation"]){
      let param = {
        clientName : this.firstFormGroup.value.clientName,
        clientManagerName: this.clnt_nm ,
        jobTitle:  this.clnt_rl ,
        phoneNumber:  this.clnt_c_no,
        email: this.clnt_email,
        draftId:data.draftId,
        ids:data.ids
      }
      if(this.isNewUserCreate){
        this.clientMng[0] =  param;
        this.isNewUserCreate = false;
        this.tblEdit = false;
      }else{
        this.tblEdit = false;
        for (let i = 0; i < this.clientMng.length; i++) {
          if(this.clientMng[i].ids == this.selectedRowIndex){
            this.clientMng[i]=  param;
            break;
          }
        }
      }
      this.dataSource = new MatTableDataSource<clientManagerIterface>(this.clientMng);
    }else{
      this.genPopup.msg = this.errClient.desc;
      this.genPopup.value = true;
    }
  }

  
  
  deleteAccountTable(){
    let isNew= false;
    for (let i = 0; i < this.clientMng.length; i++) {
      if(this.clientMng[i].ids == this.deleteIds){
        if(this.clientMng[i].draftId == ''){
          isNew = true;
          this.clientMng.splice(i,1);
        }
        else{
          this._general.setLoading(true);
          this._clientManager.deleteDraft(this.clientMng[i].draftId).subscribe(
            (res)=>{if(res){
              this._general.setLoading(false);
              this.clientMng.splice(i,1);
              this.dataSource = new MatTableDataSource<clientManagerIterface>(this.clientMng);
            }},
            (err)=>{
              this._general.setLoading(false);
              console.log(err);
            }
          )
        }
        break;
      }
    }

    for (let i = 0; i < this.newClienManager.length; i++) {
      if(this.newClienManager[i].ids == this.deleteIds){
        this.newClienManager.splice(i,1);
      }
    }
    
    this.dataSource = new MatTableDataSource<clientManagerIterface>(this.clientMng);
    this.deletePopup = false;
  }

  clientMng =[];
  clientMngOri;
  getClientManager(data){
    this.clientMng = data;
    for (let i = 0; i < data.length; i++) {
      data[i].ids=(Math.floor(Math.random() * Math.floor(1000))).toString();
   }
   this.dataSource = new MatTableDataSource<clientManagerIterface>(data);
  }

  postDraftClientManager(index){
    this._general.setLoading(true);
    this._clientManager.postDraft(this.newClienManager[index]).subscribe(
      (resp)=>{
        console.log(resp);
        if(index < this.newClienManager.length-1 ){
          index++;
          this.postDraftClientManager(index);
        }else{
          this.draftPop = true;
          this._general.setLoading(false);
        }
      },(err)=>{
        this._general.setLoading(false);
        console.log(err);
      }
    )
  }

  genPopup = {
    value:false,
    msg:""
  }
  postClientManager(index,clientID){
    this._general.setLoading(true);
    this.clientMng[index].clientId=clientID;
    this._clientManager.post(this.clientMng[index]).subscribe(
      (resp)=>{
        this.reCallPostClientManager(index, clientID);
      },(err)=>{
        this.genPopup.value = true;
        this.genPopup.msg = err.error.message;
        this.reCallPostClientManager(index, clientID);
      }
    )
  }

  reCallPostClientManager(index, clientID){
    if(index < this.clientMng.length -1 ){
      index++;
      this.postClientManager(index,clientID);
    }else{
      this.draftPop = true;
      this._general.setLoading(false);
      this.router.navigate(['/admin/task/client/client-ID'],{ queryParams: { id: clientID }} );
    }
  }

  
  //========================== end of step 2========================== 

  //========================== step 3 ==========================
  days;
  deleteTemplatePopup=false;
  dmy = this._generalClient.probationPeriodeTime;
  dmys = this._generalClient.fixedOtPer;
  timepicker= this._generalClient.time;
  ampm=this._generalClient.ampm;
  dayType=this._generalClient.otTimeType;
  hour=this._generalClient.hour;

  mon = [];
  tue = [];
  wed = [];
  thu = [];
  fri = [];
  sat = [];
  sun = [];
  
  startTimeCnf(ds){
    if(this.templates[this.activeTemplate - 1].changeTimeSetting.start.ampm == "pm"){
      this.templates[this.activeTemplate - 1].workingHours.startTime = parseInt(this.templates[this.activeTemplate - 1].changeTimeSetting.start.time) + 1200;
    }else{
      this.templates[this.activeTemplate - 1].changeTimeSetting.start.ampm = "am";
      this.templates[this.activeTemplate - 1].workingHours.startTime = parseInt(this.templates[this.activeTemplate - 1].changeTimeSetting.start.time) ;
    }
    this.templates[this.activeTemplate - 1].workingHours.startTime = this.templates[this.activeTemplate - 1].workingHours.startTime.toString();
    if(this.templates[this.activeTemplate - 1].workingHours.startTime.length == 3){
      this.templates[this.activeTemplate - 1].workingHours.startTime =  "0"+this.templates[this.activeTemplate - 1].workingHours.startTime;
    }
  }

  endTimeCnf(ds){
    if(this.templates[this.activeTemplate - 1].changeTimeSetting.end.ampm == "pm"){
      this.templates[this.activeTemplate - 1].workingHours.endTime = parseInt(this.templates[this.activeTemplate - 1].changeTimeSetting.end.time) + 1200;
    }else{
      this.templates[this.activeTemplate - 1].changeTimeSetting.end.ampm = "am";
      this.templates[this.activeTemplate - 1].workingHours.endTime = parseInt(this.templates[this.activeTemplate - 1].changeTimeSetting.end.time) ;
    }

    this.templates[this.activeTemplate - 1].workingHours.endTime = this.templates[this.activeTemplate - 1].workingHours.endTime.toString();
    if(this.templates[this.activeTemplate - 1].workingHours.endTime.length == 3){
      this.templates[this.activeTemplate - 1].workingHours.endTime =  "0"+this.templates[this.activeTemplate - 1].workingHours.endTime;
    }
  }

  setWorkingDaysPerWeek(data,i){
    this.templates[this.activeTemplate - 1].workingDaysPerWeek[i]=data;
  }

  templates=[];
  templateWorkingWeek=[];
  activeTemplate=0;
  ttlDisabled = true;

  addTemplate(){
    this.templateNullErr = false;
    this.ttlDisabled = true;
    let param = this._generalClient.addTemplate(this.templates);
    this.templates.push(param);
    this.activeTemplate = this.templates.length;
    this.mon = undefined;
    this.tue = undefined;
    this.wed = undefined;
    this.thu = undefined;
    this.fri = undefined;
    this.sat = undefined;
    this.sun = undefined;
  }

  templateChange(idx){
    this.activeTemplate = idx+1;
    this.ttlDisabled = true;
    this.mon = this.templates[this.activeTemplate-1].workingDaysPerWeek[0];
    this.tue = this.templates[this.activeTemplate-1].workingDaysPerWeek[1];
    this.wed = this.templates[this.activeTemplate-1].workingDaysPerWeek[2];
    this.thu = this.templates[this.activeTemplate-1].workingDaysPerWeek[3];
    this.fri = this.templates[this.activeTemplate-1].workingDaysPerWeek[4];
    this.sat = this.templates[this.activeTemplate-1].workingDaysPerWeek[5];
    this.sun = this.templates[this.activeTemplate-1].workingDaysPerWeek[6];
  }

  deleteTemplate(){
    this.templates.splice(this.activeTemplate-1,1);
    this.activeTemplate = 0;
    this.deleteTemplatePopup = false;
    this.ttlDisabled = true;
  }

 
  //========================== end of step 3 ========================== 
  
  draftPop=false;

  setParameter(){
    let param = {
      draftId: this.paramId,
      wowId: this.firstFormGroup.value.clientUEN,
      clientName: this.firstFormGroup.value.clientName,
      officeAddressLine1: this.firstFormGroup.value.officeAddress1,
      officeAddressLine2: this.firstFormGroup.value.officeAddress2,
      arrangement: this._generalClient.setArrangement(this.firstFormGroup.value),
      templates:this._generalClient.setTemplateParam(this.templates),
      clientManagers:this.clientMng
    }
    console.log(param);
    return param;
  }

 

  //==================== save ================================
  draftData;
  saveDraft(){
    this._general.setLoading(true);
    if(this.paramId != undefined){
      this._client.putDraft(this.setParameter()).subscribe(
        (resp)=>{
          this.draftData = resp;
            this.draftPop = true;
            this._general.setLoading(false);
        },(err)=>{
          this._general.setLoading(false);
        }
      )
    }else{ 
      this._client.postDraft(this.setParameter()).subscribe(
        (resp)=>{
          this.draftData = resp;
          this.draftPop = true;
          this._general.setLoading(false);
        },(err)=>{
          this._general.setLoading(false);
        }
      )
    }
  }

  templateNullErr = false;
  msiErr = false;
  isDone=false;
  done(){
    this._general.setLoading(true);
    let param = this.setParameter();
    let sda = false;
    this.msiErr = false;

    //sda for checking arrangement is empty or not.
    for (let i = 0; i < param.arrangement.length; i++) {
      const element = param.arrangement[i];
      if(element){sda = true;break;}
    }

    if(param.templates.length == 0){
      this.templateNullErr = true;
      this._general.setLoading( false);
      this.genPopup.value = true;
      this.genPopup.msg = "Please Add Templates";
    }else{
      this.templateNullErr = false;

      if(sda){
        this.msiErr = false;
        if(this.paramId != undefined){
          this._client.deleteDraft(param.draftId).subscribe((x)=>{
            this.postAddClient(param);
          },(err)=>{
            console.log(err);
          });
        }else{
          this.postAddClient(param);
        }
      }else{
        this.msiErr = true;
        this._general.setLoading(false);
        this.genPopup.value = true;
        this.genPopup.msg = "Please Choose MSI/ISA in Company Detail";
      }
    }

  }

  postAddClient(param){
    if(this.checkErrorValidation(param.templates)){
      this._client.post(param).subscribe((resp:LooseObject)=>{
        this._general.setLoading(false);
        let clientId = resp._id;
        this.isDone = true;
        if(this.clientMng.length > 0){
          this.postClientManager(0,clientId);
        }else{
           this.router.navigate(['/admin/task/client/client-ID'],{ queryParams: { id: clientId }} );
        }
      },(err)=>{
        this._general.setLoading(false);
        this.genPopup.value = true;
        this.genPopup.msg = "Please fill up all required fields before submitting";
        console.log(err);
        if(err.error.errmsg.substring(0,6) == "E11000"){
          this.genPopup.msg = "Client Name Already Used";
        }
      })
    }else{
      this._general.setLoading(false);
    }
  }

  templatErrorCheck=[];
  checkErrorValidation(data){
    this.templatErrorCheck = undefined;
    this.templatErrorCheck =[];

    for (let i = 0; i < data.length; i++) {
      let ss = {};
      let s = false;
      if(data[i].probationPeriod == undefined ){
        s = true; ss ={...ss, probationErr : true};
      }
      if(data[i].workingDaysPerWeek == undefined){
        s = true; ss ={...ss, workingDaysErr : true};
      }
      if(data[i].workingHours == undefined ){
        s = true; ss ={...ss, workingHoursErr : true};
      }
      if(data[i].breakTime == undefined || isNaN(data[i].breakTime)){
        s = true; ss ={...ss, breakTimeErr : true};
      }
      if(data[i].completionBonusPayment == undefined ){
        s = true; ss ={...ss, completionBonusPaymentErr : true};
      }
      if(data[i].payslipFormat == undefined ){
        s = true; ss ={...ss, payslipFormatErr : true};
      }
      if(s){
        this.activeTemplate = i;
        this.templateChange(this.activeTemplate);
      }

      this.templatErrorCheck = [...this.templatErrorCheck, ss];
    }
    
    return this.templatErrorCheck.length == 0?false:true;
  }


}


interface LooseObject {
  [key: string]: any
}


interface clientManagerIterface{
  ids:string,
  clientName: string,
  clientManagerName:string,
  jobTitle: string,
  phoneNumber: string,
  email: string,
  draftId:string
}
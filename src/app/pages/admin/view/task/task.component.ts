import { Component, OnInit, OnDestroy } from '@angular/core';
import * as $ from 'jquery';


@Component({
  selector: 'ng-admin',
  template: `
      <router-outlet></router-outlet>
  `,
  // styleUrls: ['./general.scss']
})
export class TaskComponent  implements OnInit{

  constructor() {
   }

   ngOnInit() {
    }

    ngOnDestroy(){
    }
 
}

import { Injectable } from '@angular/core';
import {AccessService } from '../access.service';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ClientmanagerService {

  constructor(
    private _access:AccessService,
    private http:HttpClient,
  ) { }
  
  url="/account/clientmanager"; 
  urlDraft="/draft/clientmanager";


  
  get(id, clientId){
    let param = {
      clientManagerId: id,
      clientId:clientId
    }
    let hdr = {
      headers : this._access.getHeader(),
      params: param 
    }
    return this.http.get(this._access.host()+this.url, hdr);
  }

  put(param){
    let hdr = {
      headers : this._access.getHeader(),
    }
    return this.http.put(this._access.host()+this.url,param, hdr);
  }

  post(param){
    let hdr = {
      headers : this._access.getHeader(),
    }
    return this.http.post(this._access.host()+this.url,param, hdr);
  }

  delete(id){
    let param = {
      clientManagerId : id
    }
    let hdr = {
      headers : this._access.getHeader(),
      body: param 
    }
    return this.http.delete(this._access.host()+this.url, hdr);
  }

  getDraft(id){
    let param = {
      draftId : id
    }
    let hdr = {
      headers : this._access.getHeader(),
      params: param 
    }
    return this.http.get(this._access.host()+this.urlDraft , hdr);
  }

  postDraft(param){
    let hdr = {
      headers : this._access.getHeader(),
    }
    return this.http.post(this._access.host()+this.urlDraft,param, hdr);
  }

  putDraft(param){
    let hdr = {
      headers : this._access.getHeader(),
    }
    return this.http.put(this._access.host()+this.urlDraft,param, hdr);
  }

  deleteDraft(id){
    let param = {
      draftId : id
    }
    let hdr = {
      headers : this._access.getHeader(),
      body: param 
    }
    return this.http.delete(this._access.host()+this.urlDraft, hdr);
  }
}

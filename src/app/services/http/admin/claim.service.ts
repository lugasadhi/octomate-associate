import { Injectable } from '@angular/core';
import {AccessService } from '../access.service';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ClaimService {

  constructor(
    private _access:AccessService,
    private http:HttpClient,
  ) { }

  api="/claim";

  get(parameter){
    let hdr = {
      headers : this._access.getHeader(),
      params: parameter 
    }
    return this.http.get(this._access.host()+this.api, hdr);
  }

  put(parameter){
    let hdr = {
      headers : this._access.getHeader(),
    }
    let params= parameter ;
    return this.http.put(this._access.host()+this.api,params, hdr);
  }

  approve(claimsGroupId){
    let hdr = {
      headers : this._access.getHeader(),
    }
    let params= {
      claimsGroupId:claimsGroupId,
      action:'verify'
    } 
    return this.http.post(this._access.host()+this.api+"/approve", params, hdr);
  }

  reject(claimsGroupId, remark){
    let hdr = {
      headers : this._access.getHeader(),
    }
    let params= {
      claimsGroupId:claimsGroupId,
      action:'reject',
      rejectionRemarks: remark
    } 
    return this.http.post(this._access.host()+this.api+"/approve", params, hdr);
  }

}

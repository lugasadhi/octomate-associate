import { TestBed } from '@angular/core/testing';

import { ConfigLeaveService } from './config-leave.service';

describe('ConfigLeaveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConfigLeaveService = TestBed.get(ConfigLeaveService);
    expect(service).toBeTruthy();
  });
});

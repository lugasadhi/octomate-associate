import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.scss']
})
export class NewLeaveComponent implements OnInit {

  constructor(
    private _actRoute:ActivatedRoute,
  ) {
    this._actRoute.queryParams
    .subscribe(params => {
      this.queryParam = params;
    });

    this._actRoute.url.subscribe(
      (url)=>{
        if(url[url.length-1].path == 'leave-ID'){
          this.isEdit = true;
          this.disableForm();
        }else{
          this.isEdit = false;
          this.isDisable = false;
          this.formConfig();
        }
      }
    )
  }


  
  options: string[] = ['One', 'Two', 'Three'];
  leaveConfig;
  supportDoc:Array<string>=[""];
  statutoryMin:Array<any>=[this.detStat()];
  isEdit=false;
  queryParam;
  isDisable;


  assignToList=['all','female','male','married with kids','married','single','singaporean','PR'];
  employeeList=['by default','manualy'];
  timeList=['Day(s)','Week(s)','Month(s)'];
  unusedLeaveDays=['Carry Forward (with Expiry)','Carry Forward (w/o Expiry)','Encash','Forfeit'];



  ngOnInit() {
  }

  detStat(){
    return {
      entitlement:{
        value:"",
        time:""
      },
      periodService:{
        value:"",
        time:""
      }
    }
  }

  formConfig(){
    this.leaveConfig = new FormGroup({
      leaveType: new FormControl(),
      unpaid: new FormControl(false),
      remarks: new FormControl(false),
      document: new FormControl(false),
      statutory : new FormControl(false),
      assignTo : new FormControl(),
      employees : new FormControl(),
      unusedLeave : new FormControl(),
    }); 
  }

  disableForm(){
    this.formConfig();
    this.isDisable = true;
    this.leaveConfig.disable();
  }

  onSubmit(){
    if(this.isEdit){
      this.updateLeave();
    }else{
      this.postLeave();
    }
    console.log(this.supportDoc);
    console.log(this.leaveConfig.value);
  }

  ngForFromInt(lg){
    let rtn = [];
    for (let i = 0; i < lg; i++) {
      rtn = [...rtn, ''];
    }
    return rtn;
  }

  addDocument(){
    this.supportDoc = [...this.supportDoc, ''];
  }

  removeDocument(idx){
    this.supportDoc.splice(idx,1);
    if(this.supportDoc.length == 0){
      this.supportDoc = [...this.supportDoc, ''];
      this.leaveConfig.value.document = false;
    }
  }

  removeStatutory(idx){
    this.statutoryMin.splice(idx,1);
    if(this.statutoryMin.length == 0){
      this.statutoryMin = [...this.statutoryMin, this.detStat()];
      this.leaveConfig.value.statutory = false;
    }
  }

  addStatutory(){
    this.statutoryMin = [...this.statutoryMin, this.detStat()];
  }
  
  edit(){
    this.isDisable = false;
    this.leaveConfig.enable();
  }


  postLeave(){}
  updateLeave(){}

  

}

 
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ClickOutsideModule } from 'ng-click-outside';
import { PdfViewerModule } from 'ng2-pdf-viewer';

import { ExportAsModule } from 'ngx-export-as';

// material
import {
  MatInputModule,
  MatTableModule,
  MatSortModule,
  MatPaginatorModule,
  MatStepperModule,
  MatRadioModule,
  MatIconModule,
  MatSelectModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatProgressSpinnerModule, 
} from '@angular/material';

import { ThemeModule } from 'src/app/@themeComponent/theme-module/theme.module';


import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { AccosiatesComponent } from './associates/accosiates.component';
import { ClaimsComponent } from './claims/claims.component';
import { LeaveComponent } from './leave/leave.component';
import { TimesheetComponent } from './timesheet/timesheet.component';
import { PayslipComponent } from './payslip/payslip.component';
import { PopupDetailPdfComponent } from './claims/popup-detail-pdf/popup-detail-pdf.component';
import { ExportExcelComponent } from './claims/export-excel/export-excel.component';
import { PopupDetailClaimsComponent } from './claims/popup-detail-claims/popup-detail-claims.component';
import { PopupDetailLeaveComponent } from './leave/popup-detail-leave/popup-detail-leave.component';
import { LeaveExportExcelComponent } from './leave/export-excel/export-excel.component';


const ADMIN_COMPONENTS = [
];

@NgModule({
  imports: [
    DashboardRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    ThemeModule,

    //other
    ClickOutsideModule,
    ExportAsModule,
    PdfViewerModule,

    // material
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatStepperModule,
    MatRadioModule,
    MatIconModule,
    MatSelectModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatProgressSpinnerModule,
    MatNativeDateModule,

  ],
  declarations: [
    ...ADMIN_COMPONENTS,
    DashboardComponent,
    AccosiatesComponent,
    ClaimsComponent,
    LeaveComponent,
    TimesheetComponent,
    PayslipComponent,
    PopupDetailPdfComponent,
    ExportExcelComponent,
    LeaveExportExcelComponent,
    PopupDetailClaimsComponent,
    PopupDetailLeaveComponent,

  ],
  exports: [],

})
export class DashboardModule {
}

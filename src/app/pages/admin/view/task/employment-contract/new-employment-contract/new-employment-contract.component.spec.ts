import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewEmploymentContractComponent } from './new-employment-contract.component';

describe('NewEmploymentContractComponent', () => {
  let component: NewEmploymentContractComponent;
  let fixture: ComponentFixture<NewEmploymentContractComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewEmploymentContractComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewEmploymentContractComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

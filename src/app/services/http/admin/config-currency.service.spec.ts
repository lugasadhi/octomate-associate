import { TestBed } from '@angular/core/testing';

import { ConfigCurrencyService } from './config-currency.service';

describe('ConfigCurrencyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConfigCurrencyService = TestBed.get(ConfigCurrencyService);
    expect(service).toBeTruthy();
  });
});

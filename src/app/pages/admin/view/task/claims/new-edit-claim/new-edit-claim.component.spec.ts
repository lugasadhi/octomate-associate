import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewEditClaimComponent } from './new-edit-claim.component';

describe('NewEditClaimComponent', () => {
  let component: NewEditClaimComponent;
  let fixture: ComponentFixture<NewEditClaimComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewEditClaimComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewEditClaimComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule} from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ClickOutsideModule } from 'ng-click-outside';

// material
import {
  MatInputModule,
  MatRadioModule,
  MatIconModule,
  MatSelectModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatNativeDateModule,
} from '@angular/material';

import { TableFilterComponent } from '../table-filter/table-filter.component';
import { FilterPipeModule } from 'ngx-filter-pipe';


const ADMIN_COMPONENTS = [
];

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,

    //other
    ClickOutsideModule,

    // material
    MatInputModule,
    MatRadioModule,
    MatIconModule,
    MatSelectModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatNativeDateModule,
    FilterPipeModule,

  ],
  declarations: [
    ...ADMIN_COMPONENTS,
    TableFilterComponent,
  ],
  exports: [
    TableFilterComponent
  ],

})
export class ThemeModule {
}

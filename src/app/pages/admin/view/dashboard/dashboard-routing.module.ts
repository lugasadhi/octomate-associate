import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { DashboardComponent } from './dashboard.component';
import { AccosiatesComponent } from './associates/accosiates.component';
import { ClaimsComponent } from './claims/claims.component';
import { LeaveComponent } from './leave/leave.component';
import { TimesheetComponent } from './timesheet/timesheet.component';
import { PayslipComponent } from './payslip/payslip.component';


const routes: Routes = [
 
  {
    path: '',
    component: DashboardComponent,
    children: [
      {
        path: 'accosiates',
        component: AccosiatesComponent,
        data: {preload: true}
      },
      {
        path: 'claims',
        component: ClaimsComponent,
        data: {preload: true}
      },
      {
        path: 'leave',
        component: LeaveComponent,
        data: {preload: true}
      },
      {
        path: 'timesheet',
        component: TimesheetComponent,
        data: {preload: true}
      },
      {
        path: 'payslip',
        component: PayslipComponent,
        data: {preload: true}
      },
      {
        path: '',
        redirectTo: 'accosiates',
        pathMatch: 'full',
        data: {preload: true}
      }, 
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  // providers:[AuthGuard]
})
export class DashboardRoutingModule {
}

import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import {FormControl} from '@angular/forms';
import {MomentDateAdapter, MAT_MOMENT_DATE_FORMATS} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import { FilterPipe } from 'ngx-filter-pipe';
import {MatDatepicker} from '@angular/material/datepicker';
import * as _moment from 'moment';
const moment =  _moment;
import {Moment} from 'moment';

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'DD/MM/YYYY',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};


@Component({
  selector: 'table-filter',
  templateUrl: './table-filter.component.html',
  styleUrls: ['./table-filter.component.scss'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class TableFilterComponent implements OnInit {

  @Input('type') type: string;
  @Input('inputType') inputType: string;
  @Input('chkBfr') chkBfr: Array<string>;
  @Input('dataList') data: Array<string>;
  @Input('search') srch: boolean;
  @Input('dateType') dateType: string;

  @Output() isOutside = new EventEmitter<number>();
  @Output() returning = new EventEmitter<any>();
  @Output() cancel = new EventEmitter<boolean>();


  dataOriCheck:Array<filterData>=[];
  dataCheck:Array<filterData>=[];
  checkReturn=[];
  claims_type;
  stDate = new FormControl();
  edDate = new FormControl();
  searchAct = false;
  firstIn=true;
  datePickC=0;
  searchText:any={data:""};
  page=1;
  isMoreClickAct=false;


  constructor() {  
  }

  ngOnInit() {
    this.dummy();
    if(this.srch != undefined){
      this.searchAct = this.srch;
    }
    setTimeout(()=>{this.firstIn = false;}, 500);
    this.dateType = this.dateType == undefined?'full':this.dateType;
  }

  chosenYearHandler(normalizedYear,data ) {
    if(data.value == null){
      data.setValue(moment());
    }
    const ctrlValue = data.value;
    ctrlValue.year(normalizedYear.year());
    return ctrlValue;
  }

  chosenMonthHandler(months , datepicker: MatDatepicker<Moment>, data) {
    const ctrlValue = data.value;
    ctrlValue.month(months.month());
    if(this.dateType == 'month'){
      ctrlValue.date(1);
      datepicker.close();
    }
    return ctrlValue;
  }

  chosenDateHandler(date, datepicker: MatDatepicker<Moment>, data){
    const ctrlValue = data.value;
    ctrlValue.date(date.date());
    datepicker.close();
    return ctrlValue;
  }

  dummy(){
    this.dataCheck = undefined;
    this.dataCheck = [];
    this.claims_type = this.type;

    if(this.inputType == 'checkbox'){
      if(this.type == 'associateId'){
        this.claims_type = 'Associate ID';
        this.associateIdQuery(this.data);
      }else{
        if(typeof this.data[0] == 'string'){
          this.otherQuery(this.data);
        }else{
          this.otherQuery2(this.data);
        }
      }
    }
    if(this.inputType == "checkbox-range"){
      this.rangeQuery(this.data);
    }
    this.checkValue();
  }

    
  datepickerAct(data){
    this.datePickC=data;
  }

  clickOutsideFilter(){
    if(!this.firstIn){
      if(this.datePickC == 0){
        this.cancelBtn();
      }
    }
    this.datePickC = this.datePickC==2?0:this.datePickC;
  }

  associateIdQuery(data){
    for (let i = 0; i < data.length; i++) {
      this.dataOriCheck = [...this.dataOriCheck, {
        data:data[i],
        showData:"#"+data[i].substring(data[i].length-6, data[i].length).toUpperCase(),
        value:false
      }]
    }
    this.page = 1;
    this.showData(this.page );
  }

  otherQuery(data){
    for (let i = 0; i < data.length; i++) {
      this.dataOriCheck = [...this.dataOriCheck, {
        data:data[i],
        showData:data[i],
        value:false
      }]
    }

    this.page = 1;
    this.showData(this.page );
  }

  otherQuery2(data){
    for (let i = 0; i < data.length; i++) {
      this.dataOriCheck = [...this.dataOriCheck, {
        data:data[i].data,
        showData:data[i].showData,
        value:false
      }]
    }

    this.page = 1;
    this.showData(this.page );
  }

  rangeQuery(data){
    for (let i = 0; i < data.length; i++) {
      this.dataCheck = [...this.dataCheck, {
        data:data[i].value,
        showData:data[i].text,
        value:false
      }]
    }
  }

  clickMore(){
    this.datePickC = 1;
    setTimeout(()=>{this.datePickC = 0;}, 500);
    this.page++;
    this.showData(this.page);
  }

  showData(page){
    let totalLengPage = Math.floor(this.dataOriCheck.length/5);
    let sisaPage = this.dataOriCheck.length%5;
    if(page <= totalLengPage){
      for (let i = (page-1)*5; i < page*5; i++) {
        this.dataCheck = [...this.dataCheck, {
          data:this.dataOriCheck[i].data,
          showData:this.dataOriCheck[i].showData,
          value:this.dataOriCheck[i].value,
        }]
      }
      this.isMoreClickAct = true;
    }else if(page == totalLengPage+1){
      for (let i = (page-1)*5; i < ((page-1)*5)+sisaPage; i++) {
        this.dataCheck = [...this.dataCheck, {
          data:this.dataOriCheck[i].data,
          showData:this.dataOriCheck[i].showData,
          value:this.dataOriCheck[i].value
        }]
      }
      this.isMoreClickAct = false;
    }else{
      this.isMoreClickAct = false;
    }
  }

  checkValue(){
    if(this.inputType == "checkbox"){
      for (let a = 0; a < this.dataCheck.length; a++) {
        for (let i = 0; i < this.chkBfr.length; i++) {
          if(this.dataCheck[a].data == this.chkBfr[i]){
            this.dataCheck[a].value = true;
            break;
          }      
        }
      }
    }else if(this.inputType == "checkbox-range"){
      let start = true;
      if(this.chkBfr[0] != undefined && this.chkBfr[1] != undefined){
        for (let s = 0; s < this.dataCheck.length; s++) {
          if(start){
            this.dataCheck[s].value= this.dataCheck[s].data[0]==this.chkBfr[0]?true:false;
            start = false;
          }else{
            this.dataCheck[s].value= this.dataCheck[s].data[1]==this.chkBfr[1]?true:false;
          }
        }
      }

      if(this.chkBfr[0] != undefined && this.chkBfr[1] == undefined){
        for (let s = 0; s < this.dataCheck.length; s++) {
          this.dataCheck[s].value = this.dataCheck[s].data[1]==undefined?true:false;
        }
      }
      
    }
    else if(this.inputType == "date-picker-range"){
      if(this.chkBfr.length > 0){
        let rs = []
        for (let i = 0; i < this.chkBfr.length; i++) {
          rs = [...rs,this.chkBfr[i].split("/")] ;
        }
        console.log(rs);
        console.log(this.chkBfr);
        
        this.stDate.setValue( moment().set({
          'year':rs[0][2],
          'month':rs[0][1],
          'date':rs[0][0]
        }));  
        this.edDate.setValue( moment().set({
          'year':rs[1][2],
          'month':rs[1][1],
          'date':rs[1][0]
        }));      
      }
    }else if(this.inputType == "date-picker"){
      let rs = []
        for (let i = 0; i < this.chkBfr.length; i++) {
          rs = [...rs,this.chkBfr[i].split("/")] ;
        }
        this.stDate.setValue( moment().set({
          'year':rs[0][0],
          'month':rs[0][1],
          'date':rs[0][2]
        }));  
    }
  }

  clickCheck(chk,data){
    for (let i = 0; i < this.dataCheck.length; i++) {
      if(this.dataCheck[i].data == data.data){
        this.dataCheck[i].value =  chk.checked;
        break;
      }
    }
  }

  apply(){
    let data;
    this.checkReturn  =[];
    if(this.inputType == 'checkbox-range'){
      let st;
      let s;

      for (let i = 0; i < this.dataCheck.length; i++) {
        if(this.dataCheck[i].value){
          st = st==undefined?this.dataCheck[i].data[0]:st;
          s = this.dataCheck[i].data[1];
        }      
      }
      if(st!= undefined){
        this.checkReturn = [st];
      }
      if(s != undefined){
        this.checkReturn = [...this.checkReturn ,s];
      }
    }else if(this.inputType == 'checkbox'){
      for (let i = 0; i < this.dataCheck.length; i++) {
        if(this.dataCheck[i].value){
          this.checkReturn = [...this.checkReturn, this.dataCheck[i].data]
        }      
      } 
    }else if(this.inputType == 'date-picker-range'){
      if((this.stDate.value != '' && this.stDate.value != undefined) && (this.edDate.value != '' && this.edDate.value != undefined) ){
        let dateStart = this.stDate.value._d;
        let dateEnd = this.edDate.value._d;
        this.checkReturn = [dateStart, dateEnd];
      }
    }else if(this.inputType == 'date-picker'){
      if((this.stDate.value != '' && this.stDate.value != undefined) && (this.edDate.value != '' && this.edDate.value != undefined) ){
        let dateStart = this.stDate.value._d;
        this.checkReturn = [dateStart];
      }
    }
  
    this.returning.emit(this.checkReturn);
    this.cancel.emit(false);
  }

  cancelBtn(){
    this.cancel.emit(false);
  }

  clearAll(){
    if(this.inputType == "checkbox"){
      for (let a = 0; a < this.dataCheck.length; a++) {
        this.dataCheck[a].value = false;
      }
    }else if(this.inputType == "date-picker-range"){
        this.stDate.setValue(null);      
        this.edDate.setValue(null);        
    }
  }


}


interface filterData {
  data:any,
  showData:string,
  value:boolean
}
import { Component, OnInit, ViewChild } from '@angular/core';


@Component({
  selector: 'app-employment-contract',
  templateUrl: './employment-contract.component.html',
  styleUrls: ['./employment-contract.component.scss']
})
export class EmploymentContractComponent implements OnInit {
  isCreateContract = false;
  constructor() { }

  totalLength={
    draft:0,
    pending:0,
    sign:0,
  }

  draftData:Array<object> = [
    {
      'client_name':"IBM Singapore Pte Ltd", 
      'job_order_name':"Contract Receptionist", 
      'associate_id':"", 
      'associate_name':"", 
      'attacment':"",
      'contractId':"1",
    },
    {
      'client_name':"IBM Singapore Pte Ltd", 
      'job_order_name':"", 
      'associate_id':"", 
      'associate_name':"", 
      'attacment':"",
      'contractId':"2",
    }
  ];

  pendingData:Array<object> = [
    {
      'ec_id':"1242124212421242", 
      'client_name':"IBM Singapore PTE LTD", 
      'job_order_name':"Contract receptionist", 
      'associate_id':"", 
      'associate_name':'',
      'action':true,
    },
    {
      'ec_id':"11242123123123242", 
      'client_name':"IBM Singapore PTE LTD", 
      'job_order_name':"", 
      'associate_id':"", 
      'associate_name':'',
      'action':false,
    }
  ]

  signData:Array<object> = [
    {
      'ec_id':"121241512442", 
      'client_name':"Singapore IBM Ple Ltd", 
      'job_order_name':"Contract Receptionist", 
      'associate_id':"", 
      'associate_name':'',
      'attachment':["file"],
    },
    {
      'ec_id':"121241212442", 
      'client_name':"", 
      'job_order_name':"", 
      'associate_id':"", 
      'associate_name':'',
      'attachment':[],
    }
  ]
  

  ngOnInit() {
    this.totalLength.draft = this.draftData.length;
    this.totalLength.pending = this.pendingData.length;
    this.totalLength.sign = this.signData.length;
  }

  tabChange(data){
    let rtn = false;
    if(data.toElement.id=='draft'){rtn = true;}
    else if(this.isCreateContract && data.toElement.id==''){rtn = true;}
    else{rtn = false;}
    this.isCreateContract = rtn;
  }

  
}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-claims',
  templateUrl: './claims.component.html',
  styleUrls: ['./claims.component.scss']
})
export class ClaimsComponent implements OnInit {

  constructor(
  
  ) { }

  displayedColumns=['_id','claimType','subClaim','description','action','deleted'];
  dataSource=[{
    _id:"001",
    claimType:"Fixed",
    subClaim:false,
    description:true,
    action:"Assign to all Employees by default",
  }]
  pageLength=5;
  pageSizeOptions: number[] = [ 15, 50, 100];

  ngOnInit() {
  }

  sortData(data){
    console.log(data);
  }
  pageEvent(data){
  }

  setPageSizeOptions(setPageSizeOptionsInput: string) {
    this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
  }

}
